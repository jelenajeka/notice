<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::any('/addTypeNotice', 'NoticeController@addTypeNotice')->name('addTypeNotice');
Route::post('/deleteType/{id?}', 'NoticeController@deleteType')->name('deleteType');
Route::any('/textFormated', 'NoticeController@textFormated')->name('textFormated');
Route::any('/addNotice', 'NoticeController@addNotice')->name('addNotice');
Route::any('/editNotice/{id?}', 'NoticeController@editNotice')->name('editNotice');
Route::any('/viewNotice/{id?}', 'NoticeController@viewNotice')->name('viewNotice');
Route::post('/deleteNotice/{id?}', 'NoticeController@deleteNotice')->name('deleteNotice');

Route::post('/searchNotice', 'NoticeController@searchNotice')->name('searchNotice');
Route::post('/noticesGroup','NoticeController@noticesGroup')->name('noticesGroup');

Route::any('/uploadFormImg', 'NoticeController@uploadFormImg')->name('uploadFormImg');
Route::any('/notSaveImage', 'NoticeController@notSaveImage')->name('notSaveImage');
Route::any('/uploadFormFile', 'NoticeController@uploadFormFile')->name('uploadFormFile');
Route::any('/notSaveFile', 'NoticeController@notSaveFile')->name('notSaveFile');
