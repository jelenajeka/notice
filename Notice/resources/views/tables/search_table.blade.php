<table id="search_table_notice" class="table table-hover">
    <thead class="thead-dark">
        <tr>
            <th scope="col">Resulet search:</th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach($notices as $n) <tr scope="row">
            <td>
                <a class="btn-action align-self-center" href="/viewNotice/{{$n['id']}}">
                    <h6 class="my-0 ml-2 text-muted w-100">{{$n['title']}}</h6>
                </a>
            </td>
            {{-- <td width="250" class="td-shadow-right text-bottom small font-weight-bold text-uppercase"><span class="badge bg-secondary text-white font-weight-bold my-0 py-0">By:</span> {{$n['noticeby']->name}} </td> --}} <td width="30"
              class="bg-light">
                <div class="d-flex justify-content-around">
                    <a class="btn-action align-self-center" href="/editNotice/{{$n['id']}}">Edit</a>
                </div>
            </td>
            <td width="30" class="bg-light">
                <div class="d-flex justify-content-around">
                    <a class="btn-action align-self-center" onclick="deleteNotice('{{$n['id']}}')" href="#"><span class="text-danger">Delete</span></a>
                    <!-- @can(Auth::user()) -->
                    <!-- @endcan -->
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
