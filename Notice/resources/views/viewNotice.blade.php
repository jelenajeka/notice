@extends('layouts.app')
@section('content')
<div class="container-fluid">
    <div class="container">
        <div class="card">
            <div class="card-header ">
                <div class="row">
                    <div class="col-md-10">
                        <h4 class="align-self-center">Notice </h4>
                        <h4 class="align-self-center">
                            {{$notice['noticetype']->type_name}} - {{$notice->title}}
                        </h4>
                    </div>
                    <div class="col-md-1">
                        <a class="btn btn-action" href="/editNotice/{{$notice['id']}}">Edit</a>
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
            <div class="card-body">
                <div class="col-md-12">
                    <div id="text_for_transform" class="px-4">{!! $notice_formatted !!}</span></div>
                </div>
            </div>
            <div class="card-footer">
                <ul class="list-unstyled my-0">
                    <span class="text-black mr-2"><i>Edited by:</i></span>
                    @foreach($editer as $e) <li class="badge bg-secondary small my-0">
                        {{$e['editedby']->name}}
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>
<script></script>
@endsection
