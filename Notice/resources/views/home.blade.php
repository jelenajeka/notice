@extends('layouts.app')
@section('content')
<div class="container-fluid">
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="align-self-center">Notice</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col md-8"></div>
                    <div class="col-md-2">
                        <a class="btn-action align-self-center" href="/addNotice">
                            <div class="info text-center">
                                {{-- <i class="fas fa-plus"></i> --}}
                                <i class="far fa-sticky-note fa-4x"></i>
                                <h4><b>New notice</b></h4>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-2">
                        <a class="btn-action align-self-center" href="/addTypeNotice">
                            <div class="info text-center">
                                <i class="fas fa-plus fa-4x"></i>
                                <h4><b>Type</b></h4>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        {{-- <input type="text" id="search_notice" placeholder="Search..." onkeyup="searchNotice()"> --}}
                        <form class="form-inline md-form form-sm mt-0">
                            <i class="fas fa-search" aria-hidden="true"></i>
                            <input class="form-control form-control-sm ml-3 w-75" type="text" placeholder="Search" aria-label="Search" onkeyup="searchNotice()">
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <ul class="list-inline typelist">
                    @foreach($types as $t) <li class="list-inline-item" value="{{$t->id}}">
                        <h6><a id="wn_g" onclick="noticesGroup('{{$t->id}}','{{ $t['type_name'] }}')">{{ $t['type_name'] }}</a></h6>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <!-- notices -->
        <div class="row">
            <div class="col-md-12">
                <table id="table_notices" class="table table-hover">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">Notices:</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($notices as $n) <tr scope="row">
                            <td>
                                <a class="btn-action align-self-center" href="/viewNotice/{{$n['id']}}">
                                    <h6 class="my-0 ml-2 text-muted">{{$n['title']}}</h6>
                                </a>
                            </td>
                            <td width="30" class="bg-light">
                                <div class="d-flex justify-content-around">
                                    <a class="btn-action align-self-center" href="/editNotice/{{$n['id']}}"><i class="fas fa-edit fa-lg"></i></a>
                                </div>
                            </td>
                            <td width="30" class="bg-light">
                                <div class="d-flex justify-content-around">
                                    <a class="btn-action align-self-center" onclick="deleteNotice('{{$n['id']}}')" href="#"><span class="text-danger"><i class="fas fa-trash fa-lg"></i></span></a>
                                    <!-- @can(Auth::user()) -->
                                    <!-- @endcan -->
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script src="/js/home.js"></script>
@endsection
