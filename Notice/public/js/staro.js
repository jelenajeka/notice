window.onload = function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function loadFile(event) {
        var output = document.getElementById('output_image');
        output.src = URL.createObjectURL(event.target.files[0]);
    };


    // $('#a').on('click', function(e)
    // {
    //   e.preventDefault();
    //   $('#amod').modal();
    // });

    $('#saveandnotsave').hide();

    $('#saveandnotsavefile').hide();

    // $('#upload').on('click', function()
    // {
    //   // $('#saveandnotsave').show();
    //   $('#saveandnotsave').display();
    //   concole.log('display');
    // })

    //Upload local image
    $('#local_img').on('click', function() {
        // $('#upload_local').empty();
        console.log('modal');
        $('#add_image').modal("show");
        // $('#upload_local').empty();
        //  $.ajax({
        //    type:'PUT',
        //    url:'/newImage',
        //    data:{'select_file':select_file},
        //    success: function(response)
        //    {
        //      $('#upload_local').append(response);
        //    }
        //  })
    })



    $('#local_img_nf').on('click', function() {
        $('#add_image_nf').modal("show");
    })

    $('#attachment').on('click', function(e) {
        e.preventDefault();
        $('#add_file').modal("show");
    })

    // $('#uploadForm').on('click', function(e)
    // {
    //   e.preventDefault();
    //   console.log('klink na upload');
    //   // $('#upload_local').empty();
    //   // var sf = $('#sf').val();
    //   // var sf = $('#sf');
    //   // var sf = $('#sf').get(0).files[0];
    //   // var sf = $('#sf')[0].files[0];
    //   // var sf = $('#sf').prop('files')[0];
    //   // var sf = document.getElementById("fileInput").files[0];
    //   // var formData = new FormData();
    //   //  formData.append('sf', sf);
    //
    //
    //   // var formData = new FormData();
    //   // formData.append('sf', $('#sf')[0].files[0]);
    //
    // //   var formData = new FormData()
    // // var file_obj = document.getElementById("sf")
    // // formData.append('sf', file_obj.files[0]);
    // // console.log(file_obj);
    //   // console.log(sf);
    //
    //   $.ajax({
    //     type:'post',
    //     url:'/uploadForm',
    //     data: {'sf':sf},
    //     success: function(response)
    //     {
    //       // $('#upload_local').empty();
    //       console.log('upload');
    //       // $('#upload_local').val(response);
    //       $('#upload_local').append(response);
    //       console.log(response);
    //       $('#add_image_nf').modal('hide');
    //       // $('#saveandnotsave').show();
    //     }
    //   })
    // })
    $('#sf').on('change', function() {
        if ($('#sf').val() != '') {
            $('#uploadForm').removeAttr("disabled");
        }
    })

    $('#buttonClose').on('click', function() {
        $('#sf').empty();
        $('#uploadForm').prop('disabled', true);
    })

    $("#add_image_form").on('submit', function(e) {
        e.preventDefault();
        var data = new FormData();
        data.append('sf', $('#sf').prop('files')[0]);

        console.log('test');
        $('#saveandnotsave').show();
        $.ajax({
            type: 'POST',
            url: '/uploadForm',
            data: data,
            contentType: false,
            cache: false,
            processData: false,
            success: function(response) {
                var d = JSON.parse(response);
                console.log(d);

                $('#saveandnotsave').show();
                $('#upload_local').empty();
                console.log('upload');
                // $('#upload_local').val(response);//ovo je okej
                $('#upload_local').val(d.img);
                console.log(d.img);
                // $('#wiki_img').append(d.id+',');//sa val hoce
                if ($('#wiki_img').val() == '') {
                    $('#wiki_img').val(d.id);
                } else {
                    var wi = $('#wiki_img').val();
                    $('#wiki_img').val(wi + ',' + d.id);
                }

                console.log(d.id);




                console.log(response);
                $('#add_image_nf').modal('hide');
                $('#sf').empty();
                $('#uploadForm').prop('disabled', true);
                console.log('disabled');

            }
        });
    });

    $('#file').on('change', function() {
        if ($('#file').val() != '') {
            $('#uploadFormFile').removeAttr("disabled");
        }
    })
    $('#buttonFile').on('click', function() {
        $('#sf').empty();
        $('#uploadFormFile').prop('disabled', true);
    })
    //submit file form
    $("#add_file_form").on('submit', function(e) {
        e.preventDefault();
        var data = new FormData();
        data.append('file', $('#file').prop('files')[0]);

        console.log('test');
        $('#saveandnotsavefile').show();
        $.ajax({
            type: 'POST',
            url: '/uploadFormFile',
            data: data,
            contentType: false,
            cache: false,
            processData: false,
            success: function(response) {
                var d = JSON.parse(response);

                $('#saveandnotsavefile').show();
                $('#upload_local_file').empty();
                console.log('upload');
                // $('#upload_local').val(response);//ovo radi
                // $('#upload_local').append(response);
                // console.log(response);
                $('#upload_local_file').val(d.file);
                console.log(d.file);
                if ($('#attach').val() == '') {
                    $('#attach').val(d.id);
                } else {
                    var wi = $('#attach').val();
                    $('#attach').val(wi + ',' + d.id);
                }

                $('#add_file').modal('hide');
                $('#file').empty();
                $('#uploadFormFile').prop('disabled', true);
                console.log('disabled');
            }
        });
    });


    $('#closemodal').on('click', function() {
        $('#add_image_nf').modal('hide');
    })

    // }
    //saveimage + markdawn 2
    $('#image_save').on('click', function(e) {
        e.preventDefault();
        var text = $('#text').val();
        var len = text.length;
        var start = $('#text').prop('selectionStart');
        var end = $('#text').prop('selectionEnd');
        var numchar = end - start;

        var upload_local = $('#upload_local').val();
        console.log(upload_local);
        var link = "![";
        var link3 = link.concat("](/img/wiki_images/");
        var link4 = link3.concat(upload_local);
        var res = link4.concat(")");
        var formatted = text.substr(0, start) + ' ' + res + text.substr(end, len);
        console.log(formatted);
        $('#text').val(formatted);
        // $('#text').append(formated);
        $('#text_formated').empty();

        $.ajax({
            type: 'post',
            url: '/textFormated',
            data: {
                'text': formatted
            },
            success: function(response) {
                $('#text_formated').append(response);
                $('#upload_local').empty();
                $('#saveandnotsave').hide();
            }
        })

    })

    //don't save
    $('#not_save').on('click', function(e) {
        e.preventDefault();
        var upload_local = $('#upload_local').val();
        $.ajax({
            type: 'post',
            url: '/notSaveImage',
            data: {
                'upload_local': upload_local
            },
            success: function(response) {
                console.log('delete image');
                console.log(response);
                $('#upload_local').empty();
                $('#saveandnotsave').hide();

                // var wiki_img = $('#wiki_img').val();
                // // var res = wiki_img.split(",");
                // var remove_after= wiki_img.indexOf(',');
                // var result =  x.substring(0, remove_after);

                if ($('#wiki_img').val() == '') {
                    $('#wiki_img').val(d.id);
                } else {
                    var wi = $('#wiki_img').val();
                    $('#wiki_img').val(wi + ',' + d.id);
                }

            }
        })

    })


    //savefale + markdawn 2
    $('#file_save').on('click', function(e) {
        e.preventDefault();
        var text = $('#text').val();
        var len = text.length;
        var start = $('#text').prop('selectionStart');
        var end = $('#text').prop('selectionEnd');
        var numchar = end - start;

        var upload_local_file = $('#upload_local_file').val();
        console.log(upload_local_file);
        var link = "[";
        var link2 = link.concat(upload_local_file);
        var link3 = link2.concat("](/wiki_files/");
        var link4 = link3.concat(upload_local_file);
        var res = link4.concat("/)");
        var formatted = text.substr(0, start) + ' ' + res + text.substr(end, len);
        console.log(formatted);
        $('#text').val(formatted);
        // $('#text').append(formated);
        $('#text_formated').empty();

        $.ajax({
            type: 'post',
            url: '/textFormated',
            data: {
                'text': formatted
            },
            success: function(response) {
                $('#text_formated').append(response);
                $('#upload_local_file').empty();
                $('#saveandnotsavefile').hide();
            }
        })
    })

    //don't save file
    $('#file_not_save').on('click', function(e) {
        e.preventDefault();
        var upload_local_file = $('#upload_local_file').val();
        $.ajax({
            type: 'post',
            url: '/notSaveFile',
            data: {
                'upload_local_file': upload_local_file
            },
            success: function(response) {
                console.log('delete file');
                console.log(response);
                $('#upload_local_file').empty();
                $('#saveandnotsavefile').hide();
            }
        })

    })
    // $('#bold').on('click', function(){
    // // e.preventDefault();
    // console.log('click');
    //  var text = $('#text').val();
    //   console.log(text);
    // });

    // function bold(e){
    $('#bold').on('click', function(e) {
        e.preventDefault();
        var text = $('#text').val();
        console.log(text);
        var len = text.length;
        var start = $('#text').prop('selectionStart');
        var end = $('#text').prop('selectionEnd');
        var numchar = end - start;
        var sel = text.substr(start, numchar);
        var bold = "**";
        var r = bold.concat(sel);
        var res = r.concat(bold);
        var formatted = text.substr(0, start) + res + text.substr(end, len);
        $('#text').val(formatted);
        $('#text_formated').empty();
        $.ajax({
            type: 'post',
            url: '/textFormated',
            data: {
                'text': formatted
            },
            success: function(response) {
                $('#text_formated').empty();
                $('#text_formated').append(response);
            }
        })
    })

    //italic
    $('#italic').on('click', function(e) {
        e.preventDefault();
        var text = $('#text').val();
        var len = text.length;
        var start = $('#text').prop('selectionStart');
        var end = $('#text').prop('selectionEnd');
        var numchar = end - start;
        var sel = text.substr(start, numchar);

        var italic = "*";
        var r = italic.concat(sel);
        var res = r.concat(italic);
        var formatted = text.substr(0, start) + res + text.substr(end, len);
        $('#text').val(formatted);
        $('#text_formated').empty();
        $.ajax({
            type: 'post',
            url: '/textFormated',
            data: {
                'text': formatted
            },
            success: function(response) {
                $('#text_formated').empty();
                $('#text_formated').append(response);
            }
        })
    })

    //link
    $('#link').on('click', function(e) {
        e.preventDefault();
        var text = $('#text').val();
        var len = text.length;
        var start = $('#text').prop('selectionStart');
        var end = $('#text').prop('selectionEnd');
        var numchar = end - start;
        var sel = text.substr(start, numchar);
        var link = "[";
        var link2 = link.concat(sel);
        var link3 = link2.concat("](//");
        var link4 = link3.concat(sel);
        var res = link4.concat("/)");
        var formatted = text.substr(0, start) + res + text.substr(end, len);
        $('#text').val(formatted);
        $('#text_formated').empty();
        $.ajax({
            type: 'post',
            url: '/textFormated',
            data: {
                'text': formatted
            },
            success: function(response) {
                $('#text_formated').empty();
                $('#text_formated').append(response);
            }
        })
    })


    //code
    $('#code').on('click', function(e) {
        e.preventDefault();
        var text = $('#text').val();
        var len = text.length;
        var start = $('#text').prop('selectionStart');
        var end = $('#text').prop('selectionEnd');
        var numchar = end - start;
        console.log(numchar);
        var sel = text.substr(start, numchar);

        var code = "`";
        var r = code.concat(sel);
        var res = r.concat(code);

        $('#text').val(res);
        var formatted = text.substr(0, start) + res + text.substr(end, len);
        $('#text').val(formatted);
        $('#text_formated').empty();

        $.ajax({
            type: 'post',
            url: '/textFormated',
            data: {
                'text': formatted
            },
            success: function(response) {
                // $('#text_formated').empty();
                $('#text_formated').append(response);
            }
        })
    })



    // //image
    // $('#image').on('click', function(e){
    //   e.preventDefault();
    //   var text = $('#text').val();
    //   var len = text.length;
    //   var start = $('#text').prop('selectionStart');
    //   var end = $('#text').prop('selectionEnd');
    //   var numchar = end-start;
    //   var sel = text.substr(start, numchar);
    //   var link = "![";
    //   var link2 = link.concat(sel);
    //   var link3 = link2.concat("](");
    //   var link4 = link3.concat("https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png");
    //   var res = link4.concat(")");
    //   var formatted = text.substr(0,start) + res + text.substr(end,len);
    //   $('#text').val(formatted);
    //   $('#text_formated').empty();
    //
    //   $.ajax({
    //      type:'post',
    //      url:'/textFormated',
    //      data:{'text':formatted},
    //      success: function(response)
    //      {
    //        $('#text_formated').append(response);
    //      }
    //    })
    // })

    //image novo
    $('#image').on('click', function(e) {
        e.preventDefault();
        var text = $('#text').val();
        var len = text.length;
        var start = $('#text').prop('selectionStart');
        var end = $('#text').prop('selectionEnd');
        var numchar = end - start;
        var sel = text.substr(start, numchar);
        var link = "![";
        var link2 = link.concat(sel);
        var link3 = link2.concat("](");

        var img = $('#output_image').attr('src');
        var link4 = link3.concat(img);
        // var link4 = link3.concat("https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png");


        var res = link4.concat(")");
        var formatted = text.substr(0, start) + res + text.substr(end, len);
        $('#text').val(formatted);
        $('#text_formated').empty();

        $.ajax({
            type: 'post',
            url: '/textFormated',
            data: {
                'text': formatted
            },
            success: function(response) {
                $('#text_formated').append(response);
            }
        })
    })

    //heading
    $('#heading').on('change', function(e) {
        e.preventDefault();
        var text = $('#text').val();
        var heading = $('#heading').val();
        var len = text.length;
        var start = $('#text').prop('selectionStart');
        var end = $('#text').prop('selectionEnd');
        var numchar = end - start;
        var sel = text.substr(start, numchar);

        if (heading == 1) {
            var heading = "#";
        }
        if (heading == 2) {
            var heading = "##";
        }
        if (heading == 3) {
            var heading = "###";
        }
        if (heading == 4) {
            var heading = "####";
        }
        if (heading == 5) {
            var heading = "#####";
        }

        var res = heading.concat(sel);
        var formatted = text.substr(0, start) + res + text.substr(end, len);
        $('#text').val(formatted);
        $('#text_formated').empty();


        $.ajax({
            type: 'post',
            url: '/textFormated',
            data: {
                'text': formatted
            },
            success: function(response) {
                $('#text_formated').append(response);
                $('#heading').val('');
            }
        })
    })

    //list
    $('#list').on('click', function(e) {
        e.preventDefault();
        var text = $('#text').val();
        var len = text.length;
        var start = $('#text').prop('selectionStart');
        var end = $('#text').prop('selectionEnd');
        var numchar = end - start;
        console.log(numchar);
        var sel = text.substr(start, numchar);

        var list = "* ";
        var res = list.concat(sel);

        $('#text').val(res);
        var formatted = text.substr(0, start) + res + text.substr(end, len);
        $('#text').val(formatted);
        $('#text_formated').empty();

        $.ajax({
            type: 'post',
            url: '/textFormated',
            data: {
                'text': formatted
            },
            success: function(response) {
                // $('#text_formated').empty();
                $('#text_formated').append(response);
            }
        })
    })


    // $('#text').on('keypress', function(e){
    //   if(e.which == 13 || e.keyCode == 13)
    //   {
    //     e.preventDefault();
    //     // var text =this.value;
    //     // var text = $('#text').val();
    //     // this.value = this.value.substring(0, this.selectionStart)+"\n"+this.value.substring(this.selectionEnd,this.value.length);
    //     // var formatted = text.substring(0, text.selectionStart)+"\n"+text.substring(text.selectionEnd,text.length);
    //     var formatted =this.value.substring(0, this.selectionStart)+"\n"+this.value.substring(this.selectionEnd,this.value.length);
    //     $('#text').val(formatted);
    //     $.ajax({
    //       type:'post',
    //       url:'/textFormated',
    //       data:{'text':formatted},
    //       success: function(response)
    //       {
    //         $('#text_formated').empty();
    //         $('#text_formated').append(response);
    //       }
    //     })
    //     e.target.value += '\n';
    //     // console.log('newRow');
    //     // $('#text_formated').append('<br>');
    //     // $('#text_formated').html('<br>');
    //   }
    // }
    $('#text').on('keypress', function(e) {
        if (e.which == 13 || e.keyCode == 13) {
            e.preventDefault();

            var formatted = this.value.substring(0, this.selectionStart) + "\n" + "\n" + this.value.substring(this.selectionEnd, this.value.length);
            $('#text').val(formatted);
            $.ajax({
                type: 'post',
                url: '/textFormated',
                data: {
                    'text': formatted
                },
                success: function(response) {
                    $('#text_formated').empty();
                    $('#text_formated').append(response);
                }
            })
        }
    })

}