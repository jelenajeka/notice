window.onload = function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // function loadFile(event) {
    //     var output = document.getElementById('output_image');
    //     output.src = URL.createObjectURL(event.target.files[0]);
    // };

    $('#saveandnotsave').hide();
    $('#saveandnotsavefile').hide();

    $('#image').on('click', function(e) {
        e.preventDefault();
        $('#add_image').modal("show");
    })
    $('#attachment').on('click', function(e) {
        e.preventDefault();
        $('#add_file').modal("show");
    })


    //skracene funkcije
    function selectText(text) {
        // var text = $('#text').val();
        var len = text.length;
        var start = $('#text').prop('selectionStart');
        var end = $('#text').prop('selectionEnd');
        var numchar = end - start;
        return numchar;
    }

    $('#img_input').on('change', function() {
        if ($('#img_input').val() != '') {
            $('#uploadFormImg').removeAttr("disabled");
        }
    })

    $("#add_image_form").on('submit', function(e) {
        e.preventDefault();
        var data = new FormData();
        data.append('img_input', $('#img_input').prop('files')[0]);

        console.log('test');
        $('#saveandnotsave').show();
        $.ajax({
            type: 'POST',
            url: '/uploadFormImg',
            data: data,
            contentType: false,
            cache: false,
            processData: false,
            success: function(response) {
                var img_id = JSON.parse(response);
                console.log(img_id);

                $('#saveandnotsave').show();
                $('#upload_local').empty();
                console.log('upload');
                // $('#upload_local').val(response);//ovo je okej
                $('#upload_local').val(img_id.img);
                console.log(img_id.img);
                // $('#wiki_img').append(d.id+',');//sa val hoce
                if ($('#img').val() == '') {
                    $('#img').val(img_id.id);
                } else {
                    var image_ids = $('#img').val();
                    $('#img').val(image_ids + ',' + img_id.id);
                }

                console.log(img_id.id);

                console.log(response);
                $('#add_image').modal('hide');
                $('#img_input').empty();
                $('#uploadFormImg').prop('disabled', true);
                console.log('disabled');

            }
        });
    });

    $('#file').on('change', function() {
        if ($('#file').val() != '') {
            $('#uploadFormFile').removeAttr("disabled");
        }
    })
    $('#buttonFile').on('click', function() {
        $('#file').empty();
        $('#uploadFormFile').prop('disabled', true);
    })
    //submit file form
    $("#add_file_form").on('submit', function(e) {
        e.preventDefault();
        var data = new FormData();
        data.append('file', $('#file').prop('files')[0]);

        console.log('test');
        $('#saveandnotsavefile').show();
        $.ajax({
            type: 'POST',
            url: '/uploadFormFile',
            data: data,
            contentType: false,
            cache: false,
            processData: false,
            success: function(response) {
                var file_id = JSON.parse(response);

                $('#saveandnotsavefile').show();
                $('#upload_local_file').empty();
                console.log('upload');
                $('#upload_local_file').val(file_id.file);
                console.log(file_id.file);
                if ($('#attach').val() == '') {
                    $('#attach').val(file_id.id);
                } else {
                    var file_ids = $('#attach').val();
                    $('#attach').val(file_ids + ',' + file_id.id);
                }

                $('#add_file').modal('hide');
                $('#file').empty();
                $('#uploadFormFile').prop('disabled', true);
                console.log('disabled');
            }
        });
    });


    $('#closemodal').on('click', function() {
        $('#add_image').modal('hide');
    })

    // }
    // saveimage + markdawn 2
    $('#image_save').on('click', function(e) {
        e.preventDefault();
        var text = $('#text').val();
        var len = text.length;
        var start = $('#text').prop('selectionStart');
        var end = $('#text').prop('selectionEnd');
        var numchar = end - start;

        var upload_local = $('#upload_local').val();
        console.log(upload_local);
        var link = "![";
        var link3 = link.concat("](/images/");
        var link4 = link3.concat(upload_local);
        var res = link4.concat(")");
        var formatted = text.substr(0, start) + ' ' + res + text.substr(end, len);
        console.log(formatted);
        $('#text').val(formatted);
        $('#text_formated').empty();

        $.ajax({
            type: 'post',
            url: '/textFormated',
            data: {
                'text': formatted
            },
            success: function(response) {
                $('#text_formated').append(response);
                $('#upload_local').empty();
                $('#saveandnotsave').hide();
            }
        })

    })

    //don't save
    $('#not_save').on('click', function(e) {
        e.preventDefault();
        var upload_local = $('#upload_local').val();
        $.ajax({
            type: 'post',
            url: '/notSaveImage',
            data: {
                'upload_local': upload_local
            },
            success: function(response) {
                console.log('delete image');
                console.log(response);
                $('#upload_local').empty();
                $('#saveandnotsave').hide();

                var img = $('#img').val();
                var res = img.split(",");
                var result = res.pop();
                $('#img').val(res);

            }
        })

    })

    //savefale + markdawn 2
    $('#file_save').on('click', function(e) {
        e.preventDefault();
        var text = $('#text').val();
        var len = text.length;
        var start = $('#text').prop('selectionStart');
        var end = $('#text').prop('selectionEnd');
        var numchar = end - start;

        var upload_local_file = $('#upload_local_file').val();
        console.log(upload_local_file);
        var link = "[";
        var link2 = link.concat(upload_local_file);
        var link3 = link2.concat("](/files/");
        var link4 = link3.concat(upload_local_file);
        var res = link4.concat("/)");
        var formatted = text.substr(0, start) + ' ' + res + text.substr(end, len);
        console.log(formatted);
        $('#text').val(formatted);
        // $('#text').append(formated);
        $('#text_formated').empty();

        $.ajax({
            type: 'post',
            url: '/textFormated',
            data: {
                'text': formatted
            },
            success: function(response) {
                $('#text_formated').append(response);
                $('#upload_local_file').empty();
                $('#saveandnotsavefile').hide();
            }
        })
    })

    //don't save file
    $('#file_not_save').on('click', function(e) {
        e.preventDefault();
        var upload_local_file = $('#upload_local_file').val();
        $.ajax({
            type: 'post',
            url: '/notSaveFile',
            data: {
                'upload_local_file': upload_local_file
            },
            success: function(response) {
                console.log('delete file');
                console.log(response);
                $('#upload_local_file').empty();
                $('#saveandnotsavefile').hide();

                var file = $('#attach').val();
                var res = file.split(",");
                var result = res.pop();
                $('#attach').val(res);

            }
        })

    })

    //heading
    $('#heading').on('change', function(e) {
        e.preventDefault();
        var text = $('#text').val();
        var heading = $('#heading').val();
        var len = text.length;
        var start = $('#text').prop('selectionStart');
        var end = $('#text').prop('selectionEnd');
        var numchar = end - start;
        var sel = text.substr(start, numchar);

        if (heading == 1) {
            var heading = "#";
        }
        if (heading == 2) {
            var heading = "##";
        }
        if (heading == 3) {
            var heading = "###";
        }
        if (heading == 4) {
            var heading = "####";
        }
        if (heading == 5) {
            var heading = "#####";
        }

        var res = heading.concat(sel);
        var formatted = text.substr(0, start) + res + text.substr(end, len);
        $('#text').val(formatted);
        $('#text_formated').empty();

        $.ajax({
            type: 'post',
            url: '/textFormated',
            data: {
                'text': formatted
            },
            success: function(response) {
                $('#text_formated').append(response);
                $('#heading').val('');
            }
        })
    })


    // })

    $('#text').on('keypress', function(e) {
        if (e.which == 13 || e.keyCode == 13) {
            e.preventDefault();

            var formatted = this.value.substring(0, this.selectionStart) + "\n" + "\n" + this.value.substring(this.selectionEnd, this.value.length);
            $('#text').val(formatted);
            $.ajax({
                type: 'post',
                url: '/textFormated',
                data: {
                    'text': formatted
                },
                success: function(response) {
                    $('#text_formated').empty();
                    $('#text_formated').append(response);
                }
            })
        }
    })
}

function clickElement(name) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var text = $('#text').val();
    console.log(text);
    var len = text.length;
    var start = $('#text').prop('selectionStart');
    var end = $('#text').prop('selectionEnd');
    var numchar = end - start;
    var sel = text.substr(start, numchar);

    switch (name) {
        case "bold":
            var bold = "**";
            var r = bold.concat(sel);
            var res = r.concat(bold);
            break;
        case "italic":
            var italic = "*";
            var r = italic.concat(sel);
            var res = r.concat(italic);
            break;
        case "link":
            var link = "[";
            var link2 = link.concat(sel);
            var link3 = link2.concat("](//");
            var link4 = link3.concat(sel);
            var res = link4.concat("/)");
            break;
        case "code":
            var code = "`";
            var r = code.concat(sel);
            var res = r.concat(code);
            break;
        case "list":
            var list = "* ";
            var res = list.concat(sel);
            break;
        default:
            // code block
    }

    var formatted = text.substr(0, start) + res + text.substr(end, len);
    $('#text').val(formatted);
    $('#text_formated').empty();
    $.ajax({
        type: 'post',
        url: '/textFormated',
        data: {
            'text': formatted
        },
        success: function(response) {
            $('#text_formated').empty();
            $('#text_formated').append(response);
        }
    })
}