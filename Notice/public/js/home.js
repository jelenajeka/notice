// $.ajaxSetup({
//     headers: {
//         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//     }
// });
function searchNotice() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var search_notice = $('#search_notice').val();
    //console.log(search_notice);
    $.ajax({
        type: 'post',
        url: '/searchNotice',
        data: {
            'search_notice': search_notice
        },
        success: function(response) {
            $('#table_notices').replaceWith(response);
        },
        error: function(response) {
            console.log("error");
        }
    })
}

function noticesGroup(group_id, title) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'post',
        url: '/noticesGroup',
        data: {
            'group_id': group_id
        },
        success: function(response) {
            $('#table_notices').replaceWith(response);
        },
        error: function(response) {
            console.log("error");
            // console.log(response);
        }
    })
}

function deleteNotice(id) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    console.log(id);
    $.post('/deleteNotice', {
        "id": id
    }, function(response) {
        if (response == "deleted") {
            console.log("delete");
            location.reload();
        } else {
            console.log("not delete");
        }
    });
}

function deleteType(id) {
    $.post('/deleteType', {
        "id": id
    }, function(response) {
        if (response == "deleted") {
            console.log(response);
            location.reload();
        } else {
            if (response == "not") {
                console.log(response);
            }
        }
    });
}
